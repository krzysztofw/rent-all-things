import express from "express";
import ProductController from "./../controller/ProductController";

const router = express.Router();

const controller = new ProductController();

router.get("/", controller.getAll);
router.post("/", controller.createNew);
router.put("/:id", controller.update);
router.delete("/:id", controller.delete);

export default router;