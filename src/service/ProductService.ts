import { PrismaClient } from '@prisma/client';

export default class ProductService {
    protected prisma: PrismaClient;

    constructor() {
        this.prisma = new PrismaClient();
    }

    public getSingle = async (productId: number) => {
        return await this.prisma.product.findFirst({
            where: { id: productId }
        });
    }

    public getAll = async () => {
        return await this.prisma.product.findMany();
    }

    public createNew = async (productData: any) => {
        const { name, price } = productData;

        return await this.prisma.product.create({
            data: {
                name,
                price: Number(price),
            },
        });
    }

    public update = async (productId: number, productData: any) => {
        const { name, price } = productData;

        return await this.prisma.product.update({
            where: { id: productId },
            data: {
                name,
                price: Number(price),
            },
        });
    }

    public delete = async (productId: number) => {
        return await this.prisma.product.delete({
            where: { id: Number(productId) }
        });
    }
}
