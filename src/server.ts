import app from "./app";
import dotenv from "dotenv";

dotenv.config();

app.set("port", process.env.PORT);

app.listen(app.get("port"), () => {
    console.log(`listening on ${app.get("port")}`);
});