import * as express from 'express';
import ProductService from "../service/ProductService";

export default class ProductController {

    protected productService: ProductService;

    constructor() {
        this.productService = new ProductService();
    }

    public getAll = async (request: express.Request, response: express.Response) => {
        const productsList = await this.productService.getAll();

        return response.json(productsList);
    }

    public createNew = async (request: express.Request, response: express.Response) => {
        const newProduct = await this.productService.createNew(request.body);

        return response.json(newProduct);
    }

    public update = async (request: express.Request, response: express.Response) => {
        const productId = request.params.id;
        const updatedProduct = await this.productService.update(Number(productId), request.body);

        return response.json(updatedProduct);
    }

    public delete = async (request: express.Request, response: express.Response) => {
        const productId = request.params.id;
        await this.productService.delete(Number(productId));

        return response.send(null);
    }
}