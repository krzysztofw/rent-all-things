import express from "express";
import BodyParser from "body-parser";
import ProductRoutes from "./routes/ProductRoutes";

const app = express();

app.use(BodyParser.urlencoded({ extended: true }));

app.use("/api/product", ProductRoutes);

export default app;